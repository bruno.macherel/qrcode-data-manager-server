// Libraries
const http = require('http')
const express = require('express')
const cors = require('cors')
const config = require('../../common/config')
const logger = require('../../common/logger')

module.exports = (async () => {
  const app = express()

  // Preventing CORS errors
  app.use(cors())

  // Middlewares
  logger.debug(`[SERVER] Initializing middleware`)
  await require('../../api/middleware')(app)

  // Handling application routes
  logger.debug(`[SERVER] Initializing routes`)
  require('../../api/routes')(app)

  const server = http.createServer(app)
  const serverConfig = config.server || {}
  const serverPort = config.PORT || serverConfig.port || 3000
  server.setTimeout(serverConfig.timeout || 2000)
  server.listen(serverPort)
  logger.info(`[SERVER] Listening on port ${serverPort}`)

  return {}
})()
