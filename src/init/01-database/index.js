const mongoose = require('mongoose')
const config = require('../../common/config')

mongoose.Promise = require('bluebird')

const { username, password, host, dbName } = config.database

module.exports = mongoose.connect(
  `mongodb://${host}/${dbName}`,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    user: username,
    pass: password
  }
)

