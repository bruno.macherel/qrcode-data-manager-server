const fs = require('bluebird').promisifyAll(require('fs'))
const logger = require('../common/logger')

try {
  const dir = 'src/init'
  const files = fs.readdirSync(dir)
  const modules = files
    .filter((file) => (file !== 'index.js'))
  for (const module of modules) { // Can't use forEach with async function
    logger.debug(`[INIT] Initialize ${module}`)
    try {
      require(`./${module}`)
    } catch (e) {
      console.error(e)
      logger.error(`[INIT] ${e}`)
    }
  }
} catch (e) {
  console.error(e)
  logger.error(`[INIT] ${e}`)
}
