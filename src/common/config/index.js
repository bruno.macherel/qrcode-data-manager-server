const nconf = require('nconf')

// Set up configs
nconf.use('memory')
// First load command line arguments
nconf.argv()
// Load environment variables
nconf.env('__')
// Load config file for the environment
nconf.file('config', './config/' + nconf.get('NODE_ENV') + '.json')

// eslint-disable-next-line no-undef
module.exports = new Proxy({}, {
  get: (target, propertyKey) => {
    return nconf.get(propertyKey)
  }
})
