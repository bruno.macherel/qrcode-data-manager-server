const winston = require('winston')
const config = require('../config')

const level = config.LOG_LEVEL
  || (config.DEBUG && (+config.DEBUG ? 'debug' : 'info'))
  || (config.NODE_ENV === 'production' && 'info')
  || 'debug'

module.exports = winston.createLogger({
  transports: [
    new winston.transports.Console({ level: level }),
    //new winston.transports.File({ filename: 'server.log' })
  ]
})
