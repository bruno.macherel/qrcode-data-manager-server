const geolib = require('geolib')
const moment = require('moment')
const { User } = require('../../models')
const { encodeValue } = require('../../../helper/codeHelper')
const { authMiddleware, authenticateUser, hasNoToken, isAdmin, isExpiredUser, isNotAdmin, isNotAuthenticated } = require('../../../helper/authHelper')

function random() {
	return Math.random();
}

function createToken(length = 16) {
	const chars = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789';
	let token = '';
	for (let i = 0; i < length; i++) {
		token += chars[Math.floor(random() * chars.length)];
	}
	return token;
}

function patchUser(user, latlng, size, version=1, detailed=true) {
	if(!user) { return undefined }
	user.lastSeen = new Date()
	user.save()
	if(!user.qrcodes) { return { qrcodes: [] } }
	if(
		latlng
		&& latlng.lat !== undefined && latlng.lat !== null
		&& latlng.lng !== undefined && latlng.lng !== null
	) {
		user.qrcodes = user.qrcodes.sort((a, b) => {
			if(!b.latlng) { return -1 }
			if(!a.latlng) { return 1 }
			const disanceA = geolib.getDistance(
				{ latitude: latlng.lat, longitude: latlng.lng },
				{ latitude: a.latlng.lat, longitude: a.latlng.lng },
			)
			const disanceB = geolib.getDistance(
				{ latitude: latlng.lat, longitude: latlng.lng },
				{ latitude: b.latlng.lat, longitude: b.latlng.lng },
			)
			return disanceA - disanceB
		})
	}
	return {
		_id: user._id,
		expiration: user.expiration,
		qrcodes: user.qrcodes.slice(0, size).map(patchCode.bind(null, version, detailed))
	}
}
function patchCode(version, detailed, code) {
	if(!code) return undefined
	const encodedValue = encodeValue(code.type, code.value, version, code.rotate) || { data: undefined, width: 0, height: 0}
	let result = {
		name: code.name,
		value: code.value,
		version,
		encodedData: encodedValue.data,
		width: encodedValue.width,
		height: encodedValue.height
	}
	if(detailed) {
		result = {
			...result,
			type: code.type,
			rotate: code.rotate,
			latlng: patchLatlng(code.latlng),
		}
	}
	return result
}
function patchLatlng(latlng) {
	return latlng && {
		lat: latlng.lat,
		lng: latlng.lng
	}
}

module.exports = {
	'/': {
		get: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAuthenticated, httpStatus: 403 },
				{ checkFct: isExpiredUser, httpStatus: 498},
				{ checkFct: isNotAdmin, httpStatus: 403 },
			]),
			async (req, res) => {
				if(req.query.schema !== undefined) {
					res.status(200)
						.json(User.jsonSchema())
				} else {
					const users = await User.find()
					res.status(200)
						.json(users)
				}
			}
		],
		// Create
		post: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken }, // Pass if the user is not authenticated
				{ checkFct: isNotAdmin, httpStatus: 403 },
			]),
			async (req, res) => {
				const requestedUser = new User({
					token: createToken(),
					name: req.body.name,
					email: req.body.email,
					creation: moment().toDate(),
				})
				if(! await isAdmin(req)) {
					requestedUser.expiration = moment().add(7, 'days').toDate()
				} else {
					if(req.body.token) {
						requestedUser.token = req.body.token
					}
				}

				const user = {
					...patchUser(await User.create(requestedUser)),
					token: requestedUser.token,
				}
				res.status(200).send(user)
			}
		]
	},
	'/:token': {
		// Read
		get: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAuthenticated, httpStatus: 403 },
				{ checkFct: isExpiredUser, httpStatus: 498},
			]),
			async (req, res) => {
				const { lat, lng, size } = req.query
				const user = patchUser(req.authenticatedUser, { lat, lng }, size, +req.query.v||1)
				res.status(200).send(user)
			},
		],
		delete: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAdmin, httpStatus: 403 },
			]),
			async (req, res) => {
				await User.findByIdAndDelete(req.params.token)
				res.status(204).send()
			},
		],
		put: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAdmin, httpStatus: 403 },
			]),
			async (req, res) => {
				await User.findByIdAndUpdate(req.body._id, req.body)
				res.status(204).send()
			},
		],
	},
	'/:token/qrcodes': {
		// Read
		get: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAuthenticated, httpStatus: 403 },
				{ checkFct: isExpiredUser, httpStatus: 498},
			]),
			async (req, res) => {
				const { lat, lng, size } = req.query
				const user = patchUser(req.authenticatedUser, { lat, lng }, size, +req.query.v||1, false)
				res.status(200).send(user.qrcodes)
			},
		],
		// Update
		put: [
			authMiddleware([{ checkFct: authenticateUser }]),
			authMiddleware([
				{ checkFct: hasNoToken, httpStatus: 401 },
				{ checkFct: isNotAuthenticated, httpStatus: 403 },
				{ checkFct: isExpiredUser, httpStatus: 498},
			]),
			async (req, res) => {
				const user = req.authenticatedUser
				user.qrcodes = req.body
				res
					.status(200)
					.json(patchUser(user).qrcodes)
			}
		]
	}
}
