// Base imports
const express = require('express')
const asyncHandler = require('express-async-handler')

const bodyParser = require('body-parser')
const morgan = require('morgan')
const fs = require('bluebird').promisifyAll(require('fs'))
const logger = require('../../common/logger')

function addRoutes (app, routeName, router, routes) {
  Object.entries(routes).forEach(([subroute, verbs]) => {
    Object.entries(verbs).forEach(([verb, controller]) => {
      if(controller instanceof Array) {
        router[verb](subroute, controller.map(c => asyncHandler(c)))
      } else {
        router[verb](subroute, asyncHandler(controller))
      }
    })
  })
  logger.debug(`[ROUTES] /${routeName}`)
  app.use(`/${routeName}`, router)
}

/*
function addRoutesFromModels (app) {
  const models = require('../models')
  Object.values(models).forEach(model => {
    const router = express.Router()
    const routeName = `${model.modelName.toLowerCase()}s`
    const routes = {
      '/': {
        // Create
        post: async (req, res) => {
          const code = await model.create(req.body)
          res
            .status(201)
            .location(`/${routeName}/${code._id}`)
            .send()
        },
        // Read
        get: async (req, res) => {
          res.json(await model.find())
        }
      },
      '/:id': {
        // Read
        get: async (req, res) => {
          res.json(await model.findById(req.params.id))
        },
        // Update
        put: async (req, res) => {
          await model.findByIdAndUpdate(req.params.id, req.body)
          res.status(200).send()
        },
        // Delete
        delete: async (req, res) => {
          await model.findByIdAndDelete(req.params.id)
          res.status(200).send()
        }
      }
    }
    addRoutes(app, routeName, router, routes)
  })
}
//*/

function addCustomRoutes(app) {
  const dir = './src/api/routes/'
  const files = fs.readdirSync(dir)
    .filter((file) => (file !== 'index.js'))
  for (const route of files) { // Can't use forEach with async function
    try {
      const router = express.Router()
      const routes = require(`./${route}`)
      addRoutes(app, route, router, routes)
    } catch (e) {
      console.error(e)
      logger.error(`[ROUTES] ${e}`)
    }
  }
}

module.exports = (app) => {
  // Logging all requests
  app.use(morgan('dev'))
  // Using BodyParser to parse requests body
  app.use(bodyParser.urlencoded({extended: true}))
  app.use(bodyParser.json())

  try {
    //addRoutesFromModels(app)
    addCustomRoutes(app)
  } catch (e) {
    logger.error(`[ROUTES] ${e}`)
    app.use((error, req, res) => {
      res.status(error.status || 500)
      res.json({
        error: {
          message: error.message,
          error: (app.get('env') === 'development' ? error : {})
        }
      })
    })
  }
}
