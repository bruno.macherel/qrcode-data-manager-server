const bwipjs = require('bwip-js')
const sharp = require('sharp')
const { encodeValue } = require('../../../helper/codeHelper')

async function resizeImage(size, buffer) {
	if(!size) {
		return buffer
	}
	return await sharp(buffer).resize(size, size).toBuffer()
}

async function generatePNG(opts) {
	const size = +opts.size
	const buffer = await bwipjs.toBuffer({
		width: 50,
		height: 50,
		...opts
	})
	return await resizeImage(size, buffer)
}

module.exports = {
	'/': {
		// Create
		get: [
			async (req, res, next) => {
				const { id, bcid, text, round } = req.query
				if(!id) {
					// There no id, it can't be a cachable request
					return next()
				}
				// Request cachable code
				return res
					.status(200)
					.json({
						id,
						data: text,
						response: encodeValue(bcid, text, 1, round).data
					}) 
			},
			async (req, res) => {
				const buffer = await generatePNG(req.query)
				res.writeHead(200, { 'Content-Type':'image/png' });
				res.end(buffer, 'binary');
			}
		],
	},
}
