const mongoose = require('mongoose')

const LatLng = new mongoose.Schema({
  lat: 'Number',
  lng: 'Number'
})
const QRCode = new mongoose.Schema({
  name: 'String',
  value: 'String',
  type: 'String',
  rotate: Number,
  latlng: LatLng
})
const User = new mongoose.Schema({
  admin: Boolean,
  name: String,
  email: String,
  token: String,
  lastSeen: Date,
  creation: Date,
  expiration: Date,
  qrcodes: [ QRCode ]
})

const UserModel = mongoose.model('User', User)
module.exports = UserModel
