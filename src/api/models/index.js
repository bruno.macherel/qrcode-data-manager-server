const mongoose = require('mongoose')
require('mongoose-schema-jsonschema')(mongoose)

const fs = require('bluebird').promisifyAll(require('fs'))

const dir = 'src/api/models/'
const jsonFileRegex = /^(.*)\.json$/
const jsFileRegex = /^(.*)\.js$/

function initJsonBasedModel() {
  return fs.readdirSync(dir)
    .filter((file) => (jsonFileRegex.exec(file)))
    .map((file) => (jsonFileRegex.exec(file)[1]))
    .reduce((models, name) => ({
      ...models,
      [name]: mongoose.model(name, new mongoose.Schema(require(`./${name}.json`)))
    }), {})
}
function initCustomModel() {
  return fs.readdirSync(dir)
  .filter((file) => (!jsonFileRegex.exec(file)))
  .filter((file) => (!jsFileRegex.exec(file)))
  .reduce((models, name) => ({
    ...models,
    [name]: require(`./${name}`)
  }), {})
}

module.exports = {
  ...initJsonBasedModel(),
  ...initCustomModel(),
}
