const logger = require('../../../common/logger')
const config = require('../../../common/config')

const timeout = config.server.bruteforce.delay
const error_count = config.server.bruteforce.count

let failures = {}
setInterval(() => {
  failures = Object.fromEntries(
    Object.entries(failures)
      .map(([key, value]) => ([key, value-1]))
      .filter(([, value]) => (value > 0))
  )
}, timeout * 1000)
function getForwardedIp(req, header) {
  const forwardedIpsStr = req.header(header)
  if (forwardedIpsStr) {
    // 'x-forwarded-for' header may return multiple IP addresses in
    // the format: "client IP, proxy 1 IP, proxy 2 IP" so take the
    // the first one
    return forwardedIpsStr.split(',')[0]
  }
}

function getClientIp(req) {
  // The request may be forwarded from local web server.
  return getForwardedIp(req, 'x-real-ip')
    || getForwardedIp(req, 'x-forwarded-for')
    || req.connection.remoteAddress
}

module.exports = (app) => {
  app.use('/', (req, res, next) => {
    const ip = getClientIp(req)

    if((failures[ip] || 0) < error_count) {
      const end = res.end
      res.end = (chunk, encoding) => {
        end.apply(res, [chunk, encoding])
        if(res.statusCode === 403) {
          failures[ip] = (failures[ip] || 0) + 1
        }
      }
      next()
    } else {
      const delay = (1 + failures[ip] - error_count) * timeout
      logger.warn(`Too many request from ${ip}. Must wait ${delay}s`)
      res.status(429).send({error: 429, message: `Too many requests. Please wait for ${delay}s`})
    }
  })
}
