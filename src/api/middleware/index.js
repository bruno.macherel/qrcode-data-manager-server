const fs = require('bluebird').promisifyAll(require('fs'))
const logger = require('../../common/logger')

module.exports = (app) => {
  try {
    const dir = 'src/api/middleware'
    const files = fs.readdirSync(dir)
    const modules = files
      .filter((file) => (file !== 'index.js'))
      .forEach((module) =>{
        logger.debug(`[MIDDLEWARE] Load ${module}`)
        try {
          require(`./${module}`)(app)
        } catch (e) {
          console.error(e)
          logger.error(`[MIDDLEWARE] ${e}`)
        }
      })
  } catch (e) {
    console.error(e)
    logger.error(`[MIDDLEWARE] ${e}`)
  }
}
