const bwipjs = require('bwip-js')
const logger = require('../common/logger')

function zip(array, ...arrays) {
	return arrays.reduce(
		(previous, current) => previous.map((e, i) => e.concat([current[i]])),
		array.map(e=>[e])
	)
}

function splitArray(length, array) {
	return array
		.map((currentValue, index, array) => array.slice(index, index + length))
		.filter((currentValue, index) => index % length === 0)
}
function transpose(matrix) {
	return matrix[0].map((col, i) => matrix.map(row => row[i]))
}

function rotate(matrix, round=0) {
	if(round<=0) {
		return matrix
	}
	return rotate(matrix[0].map((_, i) => matrix.map(row => row[i]).reverse()), round-1)
}

function encodeV1(matrix) {
	const transposed_matrix = transpose(matrix)
	const transposed_hex_matrix = transposed_matrix
			.map((row) => (
				splitArray(4, row)
					.map(array => array.join('').padEnd(4, '0'))
					.map(bin => parseInt(bin, 2))
					.map(dec => dec.toString(16))
			))
	return transpose(transposed_hex_matrix).map(row => row.join(''))
}
function encodeV2(matrix) {
	return splitArray(
		Math.ceil(matrix[0].length/2),
		splitArray(4, zip(
			...(
				matrix
					.concat(new Array(matrix.length%2).fill(new Array(matrix[0].length).fill('0')))
					.map(row => row.concat(new Array(row.length%2).fill('0')))
					.reduce((previous, current, index) => {
						previous[index%2].push(...current)
						return previous
					}, [[],[]])
					.map(e => splitArray(2, e))
			)
		).flat(2))
			.map(row => row.flat())
			.map(arr => arr.join(''))
			.map(bin => parseInt(bin, 2))
			.map(dec => dec.toString(16))
	).map(row => row.join(''))
}

function encode2d(raw, version, round=0) {
	const matrix = rotate(
		splitArray(
			raw.pixx,
			raw.pixs
		),
		round
	)
	return {
		data: version===1 ? encodeV1(matrix) : encodeV2(matrix),
		width: round%2===0 ? raw.pixx : raw.pixy,
		height: round%2===0 ? raw.pixy : raw.pixx
	}
}
function encode1d(raw, version, round=0) {
	const pixs = raw.sbs
		.filter(v => v > 0)
		.flatMap((currentValue, index) => new Array(currentValue).fill((index+1)%2))
	return encode2d({
		pixx: pixs.length,
		pixy: 1,
		pixs: new Array(4/version).fill(pixs).flat()
	}, version, round%2===0?round:0)
}
module.exports = {
	encodeValue: function(encoder, value, version=1, round=0) {
		try {
			const raw = bwipjs.raw(encoder || 'qrcode', value)[0]
			if(raw.sbs) {
				return encode1d(raw, version, round)
			}
			return encode2d(raw, version, round)
		} catch(e) {
			logger.error(`[MIDDLEWARE] ${e}`)
			console.error(e)
			return undefined
		}
	}
}