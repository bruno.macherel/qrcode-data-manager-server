const sharp = require('sharp')
const moment = require('moment')
const { User } = require('../api/models')

const getToken = (req) => {
	const token = (req.headers['x-token'] || req.query.token || req.params.token)
	return token === '${TOKEN}' ? '': token
}

const authenticateUser = async (req) => {
	const token = getToken(req)
	req.authenticatedUser = !!token && await User.findOne({token})
}

const hasNoToken = (req) => {
	return !getToken(req)
}

const isExpiredUser = (req) => {
	return !req.authenticatedUser || (req.authenticatedUser.expiration && moment(req.authenticatedUser.expiration) <= moment())
}

const isAdmin = (req) => {
	return req.authenticatedUser && req.authenticatedUser.admin
}

const isNotAdmin = (req) => {
	return !isAdmin(req)
}

const isAuthenticated = (req) => {
	return !!req.authenticatedUser
}

const isNotAuthenticated = (req) => {
	return !req.authenticatedUser
}

const __pngError_ = async (statusCode, req, res) => {
	const buffer = await sharp({
		create: {
			width: 48,
			height: 48,
			channels: 4,
			background: `hsl(${statusCode===401?30:0}, 100%, 50%)`
		}
	})
		.png()
		.toBuffer()
	
	res.writeHead(statusCode, { 'Content-Type':'image/png' });
	res.end(buffer, 'binary');
}

const __errorResponse_ = (statusCode, contentType, req, res) => {
	if(false && contentType === 'image/png') { // TODO: enable this condition when Garmin correcly check response status code
		__pngError_(statusCode, req, res)
	} else {
		res.status(statusCode).json({error: statusCode, message: 'Please use a valid token.'})
	}
}

const authMiddleware = (checks, contentType = undefined) => (async (req, res, next) => {
	// eslint-disable-next-line no-undef
	const results = await Promise.all(checks.map((check) => check.checkFct(req)))
	const index = results.findIndex(result => result)
	const httpStatus = checks.map((check) => check.httpStatus)[index]
	if(httpStatus) {
		__errorResponse_(httpStatus, contentType, req, res)
	} else {
		next()
	}
})

module.exports = {
	authMiddleware,
	authenticateUser,
	getToken,
	hasNoToken,
	isAdmin,
	isAuthenticated,
	isExpiredUser,
	isNotAdmin,
	isNotAuthenticated,
}