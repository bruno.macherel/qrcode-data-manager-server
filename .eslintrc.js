// https://eslint.org/docs/user-guide/configuring

module.exports = {
  "env": {"browser": true, "node": true},
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  // https://github.com/standard/standard/blob/master/docs/RULES-en.md
  extends: 'eslint:recommended',
  // add your custom rules here
  rules: {
    // force LF
    "linebreak-style": ["error", "unix"],
    // allow async-await
    'generator-star-spacing': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
